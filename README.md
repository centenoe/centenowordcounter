Edgar Centeno
Word Counter Project


This project will be an application that opens a text document and counts the total number of words/unique words. 
The application will also report the five most used words in the text document as well as the five least used words. 
Every word frequency will be printed in a separate text document called "EveryWordFrequency.txt" . 
This application will be written in Java. 
This program has a GUI so you can easily select the text document you want to count. 

The software design document is called WordCounterSDD.xml. (PDF included)

Currently, to run the program, click the "Choose File" button on the GUI, and choose your file. 
The results will print on the GUI, and every word frequency will be printed to EveryWordFrequency.txt


packages included:
*Java.io.File - to read files
*Java.io.FileInputStream - used in WordCounter to count words
*Java.util.Map and Java.util.HashMap to keep count of the word frequencies
*Java.util.List - to organize the values
*Java.util.Collections - sort the values
*java.util.Scanner - to take in user input and translate it to a File aswell as scan the file.
*java.util.TreeMap - organize the Hashmap in alphabetical order
*javax.swing - The GUI
*java.awt - even listeners
*java.util.FileWriter - to be able to write EveryWordFrequency.txt file
*java.util.Printwriter - to print the results on EveryWrodFrequency.txt file

Make sure that EveryWordFrequency is locked in the same folder that the src file is located (not inside the src file)