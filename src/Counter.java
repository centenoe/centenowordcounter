/**
 *
 * @author: Edgar Centeno
 */
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

/**
 *
 * The counter class will hold everything needed with counting the actual txt document
 */
public class Counter  {



    /**
     *
     * @param file - Enter the file from main
     * This will be a simple word counter, in this case punctuation does not matter.
     * This will count every word in the file
     *
     */
    public int wordCounter(File file){

        int countIndividual=0;

        try(Scanner scan = new Scanner(new FileInputStream(file))) {

            //as long as there is a word it will add it to the count
            //I added a filter to the scanner just incase
            while(scan.hasNext()){
                scan.next().toLowerCase().replaceAll("[!?,.;:|_&—^\\-]", "").replaceAll("\\]", "").replaceAll("\\[", "");
                countIndividual++;

            }
        } catch (
                FileNotFoundException e) {
            System.out.println("Error");
        }
        return countIndividual;
    }


    /**
     *
     * @param file will read the file name from main
     * @throws FileNotFoundException
     *  This will scan the entire text document and count the frequency of each word
     */

            public Map<String, Integer> wordFreq(File file) throws FileNotFoundException {

            Scanner scan = new Scanner(new File(String.valueOf(file)));

            //words contains the actual word and the count of that word (frequency)
            Map<String, Integer> words = new HashMap<>();

            //This while loop will keep going until there are no more words
            while(scan.hasNext()) {
                /* a String for every word in the map it will also lowercase every word and ignore punctuations
                   On the hamlet text that I have tested with there was a character :  "—" I do not have this on my keyboard
                   I tried recreating it -- but there is a small dash in between  "-"!= "—"
                   I am assuming "`" and "'" are unique words due to words like "wand" and "wan'd" wan'd could
                   have a different meaning / different character saying the word
                 */
                String eachWord = scan.next().toLowerCase().replaceAll("[!?,.;#%*/+<>:|_&—^\\-]", "").replaceAll("\\]", "").replaceAll("\\[", "");
                //holds the count for each word
                Integer eachCount = words.get(eachWord);

                //if the word exits add 1
                if (eachCount != null) {
                    eachCount++;
                }
                //this word has not been counted yet so start it out with 1
                else{
                    eachCount =1;
                }
                TreeMap<String, Integer> sorted = new TreeMap<>();

                //this will put the key and value in the hashMap
                words.put(eachWord, eachCount);

            }
            //returns word frequency
                return words;
        }



}
