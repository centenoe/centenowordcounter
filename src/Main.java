/**
 *
 * @author: Edgar Centeno
 * This is a word counter program that counts form text documents
 * To choose your text file click on the "Chose File" button to select your text document.
 * ---
 * This program will output:
 * the total number of words
 * The total amount of unique words
 * The top 5 most said words
 * the 5 least said words
 * print to EveryWordFrequency.txt file for every word frequency
 */

public class Main {
    /**
     * @param args This is the main method, it will output and organize the Counter class.
     */
    public static void main(String[] args) {

        //Show GUI
        GuiWindow theWindow = new GuiWindow();
        theWindow.setVisible(true);
    }
}