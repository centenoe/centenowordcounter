/*
 * Edgar Centeno
 * GUI for the Counter Program
 */


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.TreeMap;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 * The window with a couple of buttons.
 * @author Erik Steinmetz
 */
public class GuiWindow extends JFrame implements ActionListener {

        //sowing the "Choose File" button
    private JButton fileButton = new JButton("Choose File");
    //Unique words label on GUI
    private JLabel displayUniqueWords = new JLabel("");
    private JLabel uniqueWordsLabel = new JLabel( "Unique words in text: ");
    //Total words label in GUI
    private JLabel totalWordsLabel = new JLabel( "Total words in text:");
    private JLabel displayTotalWords = new JLabel("");

    private JLabel topFiveMostSaidWords = new JLabel("Top 5 most said words");
    private JLabel topFiveMostSaidWords2 = new JLabel("");
    private JLabel topFiveMostSaidWords3 = new JLabel("");
    private JLabel topFiveMostSaidWords4 = new JLabel("");
    private JLabel topFiveMostSaidWords5 = new JLabel("");

    private JLabel leastFiveSaidWords = new JLabel("Least 5 said words");
    private JLabel leastFiveSaidWords2 = new JLabel("");
    private JLabel leastFiveSaidWords3 = new JLabel("");
    private JLabel leastFiveSaidWords4 = new JLabel("");
    private JLabel leastFiveSaidWords5 = new JLabel("");



    /**
     * Creates a GuiWindow, positioning all the components.
     */
    public GuiWindow() {

        //title
        this.setTitle("Word Counter Application");

        //setting the window size
        this.setBounds(400, 600, 400, 390);
        this.getContentPane().setLayout(null);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //setting the choose file button
        this.fileButton.setBounds(75, 10, 150, 30);
        this.getContentPane().add(fileButton);
        this.fileButton.addActionListener(this);

        //showing total words
        this.totalWordsLabel.setBounds( 10, 60, 150, 30);
        this.getContentPane().add( totalWordsLabel);

        this.displayTotalWords.setBounds( 135, 60, 150, 30);
        this.getContentPane().add( displayTotalWords);


        //showing unique words
        this.uniqueWordsLabel.setBounds(10, 80, 150, 30);
        this.getContentPane().add( uniqueWordsLabel);

        this.displayUniqueWords.setBounds(135, 80, 100, 30);
        this.getContentPane().add( displayUniqueWords);


        //show top 5 list
        this.topFiveMostSaidWords.setBounds(10, 110, 400, 30);
        this.getContentPane().add( topFiveMostSaidWords);

        this.topFiveMostSaidWords2.setBounds(10, 130, 400, 30);
        this.getContentPane().add( topFiveMostSaidWords2);

        this.topFiveMostSaidWords3.setBounds(10, 150, 400, 30);
        this.getContentPane().add( topFiveMostSaidWords3);

        this.topFiveMostSaidWords4.setBounds(10, 170, 400, 30);
        this.getContentPane().add( topFiveMostSaidWords4);

        this.topFiveMostSaidWords5.setBounds(10, 190, 400, 30);
        this.getContentPane().add( topFiveMostSaidWords5);


        //show least 5 list
        this.leastFiveSaidWords.setBounds(10, 230, 400, 30);
        this.getContentPane().add( leastFiveSaidWords);

        this.leastFiveSaidWords2.setBounds(10, 250, 400, 30);
        this.getContentPane().add( leastFiveSaidWords2);

        this.leastFiveSaidWords3.setBounds(10, 270, 400, 30);
        this.getContentPane().add( leastFiveSaidWords3);

        this.leastFiveSaidWords4.setBounds(10, 290, 400, 30);
        this.getContentPane().add( leastFiveSaidWords4);

        this.leastFiveSaidWords5.setBounds(10, 310, 400, 30);
        this.getContentPane().add( leastFiveSaidWords5);



    }

    /**
     *
     * Do this when "Choose File" button has been pushed
     * JFileChooser opens, once accepted file has been chosen (txt/text file)
     * print results on GUI and on EverWordFrequency.txt and the command line
     * @throws IOException
     */
    private void fileButtonPushed() throws IOException {
        // creating the FilerChooser
        System.out.println("hit the 'Choose file' button");
        JFileChooser selectFile = new JFileChooser();
        selectFile.setFileFilter(new FileNameExtensionFilter("TEXT FILES", "txt", "text"));
        int fileAccepted = selectFile.showOpenDialog(null);
        System.out.println("The file opened: " + fileAccepted);
        if (fileAccepted == JFileChooser.APPROVE_OPTION) {

            //get the file that was selected
            File chosenFile = selectFile.getSelectedFile();
            Counter runCounter = new Counter();
            Map<String, Integer> wordFrequency = null;

            //try catch for wordFrequency
            try {
                wordFrequency = runCounter.wordFreq(chosenFile.getAbsoluteFile());
            } catch (FileNotFoundException e) {
                System.out.println("File did not pass");
            }

            //print out total word count
            System.out.println("Total word count: " + runCounter.wordCounter(chosenFile.getAbsoluteFile()));


            //calling the total word frequency
            try {
                runCounter.wordFreq(chosenFile.getAbsoluteFile());
            } catch (FileNotFoundException e) {
                System.out.println("File did not pass");
            }


            //Creating a list to organize the value of the HashMap.
            List<Integer> list = new ArrayList<>(wordFrequency.values());
            Collections.sort(list, Collections.reverseOrder()); //reverse order to start from greatest to least.

            //System.out.println(list); //uncomment if you want to see the list

            //The list size has the number of unique words used in the text file due to it having
            //the counts for every word
            System.out.println("The total amount of unique words: " + list.size() + "\n");


            //creating a TreeMap to sort wordFrequency by alphabetical order
            TreeMap<String, Integer> sortedWordFreq = new TreeMap<>();

            //alphabetical order, someone probably wants to find a word rather than see the 25th most said word.
            sortedWordFreq.putAll(wordFrequency);

            //creating the FileWriter to write every word frequency to EveryWordFrequency.txt
            FileWriter fileWriter = new FileWriter("EveryWordFrequency.txt");
            PrintWriter printWriter = new PrintWriter(fileWriter);

            //foreach loop that will go through every word and print out to the text file
            for (Map.Entry<String, Integer> entry : sortedWordFreq.entrySet()) {
                printWriter.print("Word = " + entry.getKey() + "    Count = " + entry.getValue() + "\n");
            }


            //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------
            this.displayTotalWords.setText(String.valueOf(runCounter.wordCounter(chosenFile.getAbsoluteFile())));
            this.displayUniqueWords.setText(String.valueOf(list.size()));

            /*
            using collections to get the most said word and the least said word.

            Collections.max selects the HashMap wordFrequency and then selects the max value of the HashMap and
            gets the key associated with the value of the HashMap

            I know this is a horrible implementation but it works, ill fix it if I have time.
            */
            String minKey = Collections.min(wordFrequency.entrySet(), Map.Entry.comparingByValue()).getKey();
            wordFrequency.remove(minKey);

            String minKey2 = Collections.min(wordFrequency.entrySet(), Map.Entry.comparingByValue()).getKey();
            wordFrequency.remove(minKey2);

            String minKey3 = Collections.min(wordFrequency.entrySet(), Map.Entry.comparingByValue()).getKey();
            wordFrequency.remove(minKey3);

            String minKey4 = Collections.min(wordFrequency.entrySet(), Map.Entry.comparingByValue()).getKey();
            wordFrequency.remove(minKey4);

            String minKey5 = Collections.min(wordFrequency.entrySet(), Map.Entry.comparingByValue()).getKey();
            wordFrequency.remove(minKey5);

            //Starting of maxKey
            String maxKey = Collections.max(wordFrequency.entrySet(), Map.Entry.comparingByValue()).getKey();
            wordFrequency.remove(maxKey);

            String maxKey2 = Collections.max(wordFrequency.entrySet(), Map.Entry.comparingByValue()).getKey();
            wordFrequency.remove(maxKey2);

            String maxKey3 = Collections.max(wordFrequency.entrySet(), Map.Entry.comparingByValue()).getKey();
            wordFrequency.remove(maxKey3);

            String maxKey4 = Collections.max(wordFrequency.entrySet(), Map.Entry.comparingByValue()).getKey();
            wordFrequency.remove(maxKey4);

            String maxKey5 = Collections.max(wordFrequency.entrySet(), Map.Entry.comparingByValue()).getKey();
            wordFrequency.remove(maxKey5);


            //setting the text for the GUI
            this.topFiveMostSaidWords.setText("The most said word is: '" + maxKey + "' said " + list.get(0) + " time(s)");
            this.topFiveMostSaidWords2.setText("The second most said word is: '" + maxKey2 + "' said " + list.get(1) + " time(s)");
            this.topFiveMostSaidWords3.setText("The third most said word is: '" + maxKey3 + "' said " + list.get(2) + " time(s)");
            this.topFiveMostSaidWords4.setText("The forth most said word is: '" + maxKey4 + "' said " + list.get(3) + " time(s)");
            this.topFiveMostSaidWords5.setText("The fifth most said word is: '" + maxKey5 + "' said " + list.get(4) + " time(s)");

            this.leastFiveSaidWords.setText("The least said word is: '" + minKey + "' said " + list.get(list.size() - 1) + " time(s)");
            this.leastFiveSaidWords2.setText("The second least said word is: '" + minKey2 + "' said " + list.get(list.size() - 2) + " time(s)");
            this.leastFiveSaidWords3.setText("The third least said word is: '" + minKey3 + "' said " + list.get(list.size() - 3) + " time(s)");
            this.leastFiveSaidWords4.setText("The fourth said word is: '" + minKey4 + "' said " + list.get(list.size() - 4) + " time(s)");
            this.leastFiveSaidWords5.setText("The fifth said word is: '" + minKey5 + "' said " + list.get(list.size() - 5) + " time(s)");

        } else {
            System.out.println("You hit cancel");
        }

    }
    /**
     * Respond to button pushes
     * @param event The action event, including the button command.
     */
    public void actionPerformed( ActionEvent event) {
        System.out.println("The action event is " + event);
        if ( event.getActionCommand().equals("Choose File")) {
            try {
                this.fileButtonPushed();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }

    }
}
